<?php

namespace App\Nova;

use GeneralSystemsVehicle\JustReadTheInstructions\JustReadTheInstructions;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Orders extends Resource
{
    public static function indexQuery(NovaRequest $request, $query)
    {
        $query->when(empty($request->get('orderBy')), function ($q) {
            $q->getQuery()->orders = [];
            return $q->orderBy(static::$model::orderColumnName());
        });

        return $query;
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Orders';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'order_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'order_name', 'reference_code'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Name', 'order_name')->sortable()->hideWhenCreating()->hideWhenUpdating(),
            Number::make('Price', 'price')->sortable()->hideWhenCreating()->hideWhenUpdating(),
            Number::make('Reference Code', 'reference_code')->sortable()->hideWhenCreating()->hideWhenUpdating(),
            Select::make(__('Status'), 'status')
                ->options([
                    'NEW ORDER' => 'NEW ORDER',
                    'PROCESSING' => 'PROCESSING',
                    'MERGED' => 'MERGED',
                    'READY' => 'READY',
                    'SPECIAL SERVICE' => 'SPECIAL SERVICE',
                ]),
            BelongsTo::make('User', 'user', 'App\Nova\User')->hideWhenCreating()->hideWhenUpdating(),

        ];
    }
    public function serializeForIndex(NovaRequest $request, $fields = null)
    {
        return array_merge(parent::serializeForIndex($request, $fields), [
            'sortable'	=> true
        ]);
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new \App\Nova\Metrics\Orders(),
            new \App\Nova\Metrics\OrdersByDate(),

        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
