<?php

namespace App;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Ofcold\NovaSortable\SortableTrait;

class Orders extends Model
{
    use SortableTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['reference_code', 'status', 'order_name', 'price', 'user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function getCreatedAtAttribute($dates)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $dates)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($dates)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $dates)->format('Y-m-d');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $length = 6;
            $faker = Factory::create();
            $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
            $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $model->reference_code = $randomString;
            $model->order_name = $faker->productName;
            $model->user_id = 1;
            $model->price = rand(12 * 10, 1000 * 10) / 10;
        });

    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
