<p class="mt-8 text-center text-xs text-80">
    <a href="https://coder79.online" class="text-primary dim no-underline">Vuejs Dashboard</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} coder79
    <span class="px-1">&middot;</span>
    v 1.0
</p>
